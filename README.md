# adm-pelis-gstd

Ejercicio de programación.
Se usa Docker para ejecutar los servicios PHP con NGINX, PostgreSQL y Composer.
El directorio src contiene Laravel 7.x y VueJs 2.6.x.

## General

Esta configuración de Docker Compose ejecuta 3 servicios:

* nginx (NGINX 1.19.2)
* php (PHP 7.3.23 with PHP-FPM)
* postgres (PostgreSQL 12.4)

La imagen PHP viene con las extensiones requeridas para Laravel 7.x (Back-End) y está configurada con xdebug.
Composer se ejecuta en el momento del arranque e instala automáticamente los paquetes requeridos del proyecto; también se hace la migración de las tablas de BD (migrate y seed) y los paquetes con NPM para Vue (Front-End).

## Requisitos para la instalación

Antes de proceder es necesario tener instalado o instalar:
- [Docker Desktop](https://www.docker.com/products/docker-desktop) ^2.3 (Docker Engine ^19.03.12)

Docker Desktop ya incluye Docker Engine, necesario para crear los contenedores de servicios.

- Una vez instalado (o si ya lo tenías instalado), ejecuta Docker Desktop para que el demonio del servicio esté disponible (comandos: docker y docker-compose) 

## Cómo usarlo y probarlo

### Iniciando Docker Compose para levantar todo el proyecto

Para levantar el proyecto sólo se requiren estos 5 pasos.

1. Renombrar los archivos `.env.example` tanto de la carpeta raíz como en `src` y dejarlos como `.env`


2. En la raíz de este repositorio (adm-pelis-gstd), ejecuta:

`docker-compose up -d`

Si ya habías intentado sin éxito (porque surgió un error) construir los contenedores con el comando anterior y no funcionó, puedes ejecutar el siguiente para empezar de nuevo (reconstruir los contenedores):

`docker-compose up -d --force-recreate --no-deps --build`

Lo anterior tomará el archivo de configuración de Docker Compose para construir los contenedores; tarda aproximadamente 5-10 minutos, dependiendo del equipo de cómputo.


3. Una vez construidos los contenedores y se hayan levantado los servicios, verás algo como esto:

```
Successfully tagged service_php:latest
Successfully tagged service_nginx:latest
Successfully tagged service_postgres:latest

Creating postgres-service ... done
Creating php-service      ... done
Creating nginx-service    ... done
```

Si es así, ejecuta lo siguiente para entrar a la terminal del servicio de PHP:

`docker exec -it php-service bash`


4. Ya estando en la terminal del contenedor de PHP, ejecuta lo siguiente para preparar Laravel y Vue:

`composer install && php artisan migrate && composer dump-autoload && php artisan db:seed && npm install && npm run dev`

Lo anterior instalará las dependencias PHP del proyecto, migrará las tablas de base de datos y registros de inicio para Laravel, así como la instalación de las dependencias de Vue y la empaquetación.

5. El proyecto estará disponible en: `http://localhost`

Es todo, al entrar a esa dirección en el navegador sólo debes seguir las instrucciones.

Si deseas detener los servicios, ejecuta esto: `docker-compose down`, sólo ten en cuenta que los datos de la BD desaparecerán por no ser persistentes en la configuración; sigue el paso 4 para migrar los datos otra vez.

Si al entrar a la aplicación en el navegador te aparece un error de Laravel por permisos de archivo, asegúrate de asignar permisos de escritura a los directorios y sus contenidos de Laravel: `storage` y `bootstrap/cache`.


# Levantar Laravel (migrar tablas, establecer semillas, instalar npm y compilar)

### Usando comandos de docker

Adicionalmente si lo deseas, puedes interactuar con los servicios de los contenedores, para ello va una información útil.

Para poder entrar a la terminal de cada contenedor debes ejecutar esto en la terminal del sistema (anfitrión).
Para NGINX (servidor web): `docker exec -it nginx-service bash`
Para PHP (lenguaje): `docker exec -it php-service bash`
Para PostgreSQL (base de datos): `docker exec -it postgres-service bash`

### Usando npm para construir la App con Vue

`npm run dev` o `npm run prod`

Si deseas hacer modificaciones al código y quieres ver los cambios, ejecuta: `npm run watch`

## Cambiar las configuraciones

El proyecto principal, donde están los archivos de Docker y el src (source) con Laravel y Vue, contienen archivos de configuración `.env` que puedes modificar para hacer pruebas.
