@extends('layouts.app')
@section('content')
    @component('components.generalpage')
        @component('components.card')
            @slot('title')
                Bienvenido al Administrador de Películas
            @endslot
            <div class="content">
                <p>Inicia sesión o registra un nuevo usuario.</p>
            </div>
            <hr>
            <div class="buttons">
                <a href="{{ route('login') }}" class="button is-black">Iniciar sesión</a>
                <a href="{{ route('register') }}" class="button is-black is-outlined">Registrarme</a>
            </div>
        @endcomponent
    @endcomponent
@endsection
