<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="@stack('html-class')">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        {{-- Scripts --}}
        @stack('head-scripts')
        {{-- Fonts --}}
        <link rel="dns-prefetch" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css?family=Oxygen" rel="stylesheet" type="text/css">
        {{-- Styles --}}
        <link href="{{ mix($stylesheet ?? 'css/app.css') }}" rel="stylesheet">
        <link href="{{ mix('css/vendor.css') }}" rel="stylesheet">
        <title>{{ config('app.name') }}</title>
        @stack('head-after')
    </head>
    <body>
        <div id="app">
            @yield('content')
        </div>
        @stack('bottom')
    </body>
</html>
