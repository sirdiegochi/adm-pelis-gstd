@extends('layouts.app')
@section('content')
    @component('components.generalpage')
        @component('components.card')
            @slot('title')
                Registrarme
            @endslot
            <form action="{{ route('register') }}" method="post">
                @csrf
                <div class="field">
                    <label class="label" for="email">Nombre</label>
                    <div class="control">
                        <input id="name" type="text" class="input @error('name') is-danger @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                    </div>
                    @error('name')
                        <p class="help is-danger" role="alert">
                            {{ $message }}
                        </p>
                    @enderror
                </div>
                <div class="field">
                    <label class="label" for="email">Correo:</label>
                    <div class="control">
                        <input id="email" type="email" class="input @error('email') is-danger @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>
                    </div>
                    @error('email')
                    <p class="help is-danger" role="alert">
                        {{ $message }}
                    </p>
                    @enderror
                </div>
                <div class="field">
                    <label class="label" for="password">Contraseña:</label>
                    <div class="control">
                        <input id="password" type="password" class="input @error('password') is-danger @enderror" name="password" required autocomplete="new-password" autofocus>
                    </div>
                    @error('password')
                    <p class="help is-danger" role="alert">
                        {{ $message }}
                    </p>
                    @enderror
                </div>
                <div class="field">
                    <label class="label" for="password-confirm">Confirmar contraseña:</label>
                    <div class="control">
                        <input id="password-confirm" type="password" class="input" name="password_confirmation" required autocomplete="new-password" autofocus>
                    </div>
                </div>
                <hr>
                <div class="field is-form-action-buttons">
                    <button type="submit" class="button is-black">Registrarme</button>
                    <div class="is-pulled-right">
                        <a href="{{ route('login') }}" class="button is-black is-outlined">Iniciar sesión</a>
                    </div>
                </div>
            </form>
        @endcomponent
    @endcomponent
@endsection
