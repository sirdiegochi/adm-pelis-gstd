@extends('layouts.app')
@section('content')
    @component('components.generalpage')
        @component('components.card')
            @slot('title')
                Iniciar sesión
            @endslot
            <p>
                Para una prueba rápida puedes usar la cuenta de prueba.<br>
                Correo: <strong>usuario@prueba.mx</strong><br>
                Contraseña: <strong>diegodev</strong>
            </p>
            <br>
            <form action="{{ route('login') }}" method="post">
                @csrf
                <div class="field">
                    <label class="label" for="email">Correo:</label>
                    <div class="control">
                        <input id="email" type="email" class="input @error('email') is-danger @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                    </div>
                    @error('email')
                        <p class="help is-danger" role="alert">
                            {{ $message }}
                        </p>
                    @enderror
                </div>
                <div class="field">
                    <label class="label" for="password">Contraseña:</label>
                    <div class="control">
                        <input id="password" type="password" class="input @error('password') is-danger @enderror" name="password" required autocomplete="current-password" autofocus>
                    </div>
                    @error('password')
                        <p class="help is-danger" role="alert">
                            {{ $message }}
                        </p>
                    @enderror
                </div>
                <div class="control">
                    <label tabindex="0" class="b-checkbox checkbox is-thin">
                        <input type="checkbox" value="false" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                        <span class="check is-black"></span>
                        <span class="control-label">Recordarme</span>
                    </label>
                </div>
                <hr>
                <div class="field is-form-action-buttons">
                    <button type="submit" class="button is-black">Entrar</button>
                    <div class="is-pulled-right">
                        <a href="{{ route('register') }}" class="button is-black is-outlined">Registrarme</a>
                    </div>
                </div>
            </form>
        @endcomponent
    @endcomponent
@endsection
