require('./bootstrap');

// Vue y Buefy (con Bulma)
import Vue from "vue"
import Buefy from "buefy"
// Router
import router from "./router"
import store from "./store"
// App
import App from "./App.vue"
import AsideMenuList from "@/components/AsideMenuList"

// Menú lateral colapsado
router.afterEach(() => {
  store.commit("asideMobileStateToggle", false)
})

// Config. Vue
Vue.config.productionTip = false
Vue.component("AsideMenuList", AsideMenuList)
Vue.component("App", App)
Vue.use(Buefy)

// Nueva instancia de Vue
new Vue({
  store,
  router,
  render: h => h(App),
  mounted() {
    document.documentElement.classList.remove("has-spinner-active")
  }
}).$mount("#app")
