import Vue from "vue"
import Router from "vue-router"
import Home from "./views/Home.vue"

Vue.use(Router)

export default new Router({
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "home",
      component: Home
    },
    {
      path: "/movies/index",
      name: "movies.index",
      component: () => import("./views/Movies/MoviesIndex.vue"),
    },
    {
      path: "/movies/new",
      name: "movies.new",
      component: () => import("./views/Movies/MoviesForm.vue"),
    },
    {
      path: "/movies/:id",
      name: "movies.edit",
      component: () => import("./views/Movies/MoviesForm.vue"),
      props: true
    },
    {
      path: "/schedules/index",
      name: "schedules.index",
      component: () => import("./views/Schedules/SchedulesIndex.vue"),
    },
    {
      path: "/schedules/new",
      name: "schedules.new",
      component: () => import("./views/Schedules/SchedulesForm.vue"),
    },
    {
      path: "/schedules/:id",
      name: "schedules.edit",
      component: () => import("./views/Schedules/SchedulesForm.vue"),
      props: true
    },
    {
      path: "/admins/index",
      name: "admins.index",
      component: () => import("./views/Admins/AdminsIndex.vue"),
    },
    {
      path: "/profile",
      name: "profile",
      component: () => import("./views/Profile.vue")
    }
  ],
  scrollBehavior (to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition
    } else {
      return { x: 0, y: 0 }
    }
  }
})
