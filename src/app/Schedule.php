<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    protected $appends = [
        'created'
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    protected $fillable = [
        'hour',
        'status'
    ];

    protected $dates = [
        'hour'
    ];

    public function getHourOnlyAttribute() {
        if (empty($this->hour)) {
            return null;
        }

        return $this->hour->format('H:i');
    }

    public function getCreatedAttribute() {
        if (empty($this->created_at)) {
            return null;
        }

        return $this->created_at->toFormattedDateString();
    }

    public function getCreatedDMYAttribute() {
        if (empty($this->created_at)) {
            return null;
        }

        return $this->created_at->format('d/m/Y');
    }

    public function setCreatedDateAttribute( $value ) {
        try {
            $this->attributes['created_at'] = new Carbon($value);
        } catch (\Exception $exception) {
            $this->attributes['created_at'] = now();
        }
    }

    public function setStatusAttribute( $value ) {
        $this->attributes['status'] = $value ? $value : '0';
    }
}
