<?php


namespace App\Processors;


class CoverProcessor {
    public static function get( $model ) {
        if (empty($model->file)) {
            if (!empty($model->name)) {
                return '/images/movie-cover.png';
            }

            return null;
        }

        return $model->file->url;
    }
}
