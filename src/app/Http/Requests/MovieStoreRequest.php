<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MovieStoreRequest extends FormRequest
{
    /**
     * Autorización para la petición
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Reglas de validación
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'date_published' => 'required'
        ];
    }
}
