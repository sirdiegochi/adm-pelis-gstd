<?php

namespace App\Http\Controllers;

use App\Movie;
use App\Http\Requests\MovieStoreRequest;
use Illuminate\Http\Request;

class MoviesController extends Controller
{
    /**
     * Instancia de controlador
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Lista de películas
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index() {
        $movies = Movie::with('file')->get();

        $movies->each(function ($movie) {
            $movie->append('cover');
            $movie->append('date_published_dmy');
        });

        return response()->json([
            'data' => $movies
        ]);
    }

    /**
     * Total
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function total() {
        $total = Movie::with('file')->count();

        return response()->json([
            'data' => ['total' => $total]
        ]);
    }

    /**
     * Obtener una película
     *
     * @param Movie $movie
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show( Movie $movie ) {
        $movie->append('cover');
        $movie->append('cover_filename');
        $movie->append('date_published_dmy');
        $movie->append('created_dmy');

        return response()->json([
            'data' => $movie
        ]);
    }

    /**
     * Editar una película
     *
     * @param MovieStoreRequest $request
     * @param Movie $movie
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update( MovieStoreRequest $request, Movie $movie ) {
        $movie->fill($request->all());
        $movie->save();

        $movie->append('cover');
        $movie->append('cover_filename');
        $movie->append('date_published_dmy');
        $movie->append('created_dmy');

        return response()->json([
            'status' => true,
            'data' => $movie
        ]);
    }

    /**
     * Guardar una película
     *
     * @param MovieStoreRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store( MovieStoreRequest $request ) {
        $movie = new Movie;
        $movie->fill($request->all());
        $movie->save();

        return response()->json([
            'status' => true,
            'created' => true,
            'data' => [
                'id' => $movie->id
            ]
        ]);
    }

    /**
     * Eliminar una película
     *
     * @param Movie $movie
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy( Movie $movie ) {
        $movie->delete();

        return response()->json([
            'status' => true
        ]);
    }
}
