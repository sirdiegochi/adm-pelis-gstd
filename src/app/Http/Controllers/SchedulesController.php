<?php

namespace App\Http\Controllers;

use App\Schedule;
use App\Http\Requests\ScheduleStoreRequest;
use Illuminate\Http\Request;

class SchedulesController extends Controller
{
    /**
     * Instancia de controlador
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Lista de turnos
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index() {
        $schedules = Schedule::get();

        $schedules->each(function ($schedule) {
            $schedule->append('hour_only');
        });

        return response()->json([
            'data' => $schedules
        ]);
    }

    /**
     * Obtener un turno
     *
     * @param Schedule $schedule
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show( Schedule $schedule ) {
        $schedule->append('created_dmy');
        $schedule->append('hour_only');

        return response()->json([
            'data' => $schedule
        ]);
    }

    /**
     * Editar un turno
     *
     * @param ScheduleStoreRequest $request
     * @param Schedule $schedule
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update( ScheduleStoreRequest $request, Schedule $schedule ) {
        $schedule->fill($request->all());
        $schedule->save();

        $schedule->append('created_dmy');
        $schedule->append('hour_only');

        return response()->json([
            'status' => true,
            'data' => $schedule
        ]);
    }

    /**
     * Guardar un turno
     *
     * @param ScheduleStoreRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store( ScheduleStoreRequest $request ) {
        $schedule = new Schedule;
        $schedule->fill($request->all());
        $schedule->save();

        return response()->json([
            'status' => true,
            'created' => true,
            'data' => [
                'id' => $schedule->id
            ]
        ]);
    }

    /**
     * Eliminar un turno
     *
     * @param Schedule $schedule
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy( Schedule $schedule ) {
        $schedule->delete();

        return response()->json([
            'status' => true
        ]);
    }
}
