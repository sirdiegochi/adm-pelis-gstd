<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Requests\ProfileUpdateRequest;

class LoggedinUserController extends Controller
{
    /**
     * Instancia de controlador
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Obtener perfil de usuario
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show( Request $request ) {
        $user = $request->user();

        return response()->json([
            'data' => $request->user()
        ]);
    }

    /**
     * Actuaizar datos de perfil
     *
     * @param ProfileUpdateRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update( ProfileUpdateRequest $request ) {
        $user = $request->user();

        $user->fill($request->only(['name', 'email']));
        $user->save();

        return response()->json([
            'data' => $user
        ]);
    }
}
