<?php

namespace App;

use App\Processors\CoverProcessor;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Movie extends Model
{
    protected $appends = [
        'created'
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'file_id',
        'file'
    ];

    protected $fillable = [
        'name',
        'date_published',
        'status',
        'file_id'
    ];

    protected $dates = [
        'date_published'
    ];

    public function file() {
        return $this->belongsTo(File::class);
    }

    public function getCoverFilenameAttribute() {
        if (!empty($this->file)) {
            return $this->file->name;
        }

        return null;
    }

    public function getCoverAttribute() {
        return CoverProcessor::get($this);
    }

    public function getDatePublishedDmyAttribute() {
        if (empty($this->date_published)) {
            return null;
        }

        return $this->date_published->format('d/m/Y');
    }

    public function getCreatedAttribute() {
        if (empty($this->created_at)) {
            return null;
        }

        return $this->created_at->toFormattedDateString();
    }

    public function getCreatedDmyAttribute() {
        if (empty($this->created_at)) {
            return null;
        }

        return $this->created_at->format('d/m/Y');
    }

    public function setCreatedDateAttribute( $value ) {
        try {
            $this->attributes['created_at'] = new Carbon($value);
        } catch (\Exception $exception) {
            $this->attributes['created_at'] = now();
        }
    }
}
