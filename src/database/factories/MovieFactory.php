<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Movie;
use Faker\Generator as Faker;

$factory->define(Movie::class, function (Faker $faker) {
    $topmovies = [
        'Parásitos',
        'Retrato de una mujer en llamas',
        'Diamantes en bruto',
        '1917',
        '5 Sangres',
        'El faro',
        'El hombre invisible',
        'Queen y Slim: los fugitivos',
        'Unidos',
        'Nunca, casi nunca, a veces, siempre',
    ];
    return [
        'name' => $faker->unique()->randomElement($topmovies),
        'date_published' => $faker->dateTimeBetween('-3650 days', '-' . rand(365, 3650) . ' days'),
        'created_at' => now()
    ];
});
