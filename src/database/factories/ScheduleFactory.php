<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Schedule;
use Faker\Generator as Faker;

$factory->define(Schedule::class, function (Faker $faker) {
    $datehours = [];
    $currentdate = date('Y-m-d');
    $starthour = new \DateTime('10:00');

    for ($i = 10; $i < 23; $i++) {
        $datehours[] = $currentdate . ' ' . $starthour->add(new \DateInterval('PT30M'))->format('H:i');
    }

    return [
        'hour' => $faker->unique()->randomElement($datehours),
        'created_at' => now()
    ];
});
