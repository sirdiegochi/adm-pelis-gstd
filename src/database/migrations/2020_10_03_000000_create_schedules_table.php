<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchedulesTable extends Migration
{
    /**
     * Turnos
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schedules', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamp('hour')->unique();
            $table->string('status')->default('0');
            $table->timestamps();
        });
    }

    /**
     * Revertir migración
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schedules');
    }
}
