<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFilesTable extends Migration
{
    /**
     * Archivos de imagen
     *
     * @return void
     */
    public function up()
    {
        Schema::create('files', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('file');
            $table->string('name')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Revertir migración
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('files');
    }
}
