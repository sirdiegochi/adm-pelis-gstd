<?php

use Illuminate\Database\Seeder;

class MoviesTableSeeder extends Seeder
{
    /**
     * Semilla de BD
     *
     * @return void
     */
    public function run()
    {
        factory(App\Movie::class, 10)->create();
    }
}
