<?php

use Illuminate\Database\Seeder;

class SchedulesTableSeeder extends Seeder
{
    /**
     * Semilla de BD
     *
     * @return void
     */
    public function run()
    {
        factory(App\Schedule::class, 10)->create();
    }
}
