<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Semilla de BD
     *
     * @return void
     */
    public function run()
    {
        \App\User::create([
            'name' => 'Usuario Prueba',
            'email' => 'usuario@prueba.mx',
            'password' => bcrypt('diegodev')
        ]);
    }
}
