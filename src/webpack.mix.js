const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// Alias de Laravel Mix Alias para usar @
require("laravel-mix-alias");

mix
  .alias({
    "@": "/resources/js",
    "@/components": "/resources/js/components",
  })
  .js("resources/js/app.js", "public/js")
  .sass("resources/sass/app.scss", "public/css")
  .styles([
    "node_modules/@mdi/font/css/materialdesignicons.css",
  ], "public/css/vendor.css")
  .copyDirectory("node_modules/@mdi/font/fonts", "public/fonts")
  .copyDirectory("resources/images", "public/images")
  .version();
