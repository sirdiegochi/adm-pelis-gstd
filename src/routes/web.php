<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// Sección películas
Route::prefix('/movies')->group(function () {
    Route::get('', 'MoviesController@index');
    Route::get('/total', 'MoviesController@total');
    Route::get('{movie}', 'MoviesController@show');
    Route::post('store', 'MoviesController@store');
    Route::patch('{movie}', 'MoviesController@update');
    Route::delete('{movie}/destroy', 'MoviesController@destroy');
});

// Sección turnos (horarios)
Route::prefix('/schedules')->group(function () {
    Route::get('', 'SchedulesController@index');
    Route::get('{schedule}', 'SchedulesController@show');
    Route::post('store', 'SchedulesController@store');
    Route::patch('{schedule}', 'SchedulesController@update');
    Route::delete('{schedule}/destroy', 'SchedulesController@destroy');
});

// Sección usuarios
Route::prefix('/user')->group(function () {
    Route::get('', 'LoggedinUserController@show');
    Route::patch('', 'LoggedinUserController@update');
});

// Subir archivos de imagen para las portadas de las películas
Route::post('/files/store', 'FilesController@store');
